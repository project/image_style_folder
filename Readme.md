This is a simple and small module. The prupose of it is to set a folder and some image styles. The slected image styles are then generated to that folder on "node save".
You want to fo that if you have images that are "protected" from a direct download, like big derivates of an image.

The module has been written for www.texturecase.com a texture database.


Hooks
-------
Internally we use these hooks:
* hook_entity_save: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!entity.api.php/function/hook_entity_update/8.9.x
* hook_entity_insert: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!entity.api.php/function/hook_entity_insert/8.9.x


Idea
----
The idea is that we can generate these styles easily using views VBO operations.


Similar Modules
--------------
Generates image styles: https://www.drupal.org/project/image_style_warmer


Info
-----
I have tested the module using the private file path i have configured accodring to the docs. In theory other paths are possible if write access is possible.
