<?php

namespace Drupal\image_style_folder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ImageStyleFolderSettings extends ConfigFormBase {


function getEditableConfigNames() {

  return [
    'image_style_folder.settings',
  ];
}

function getFormId() {
  return 'image_style_folder_settings';
}

function buildForm(array $form, FormStateInterface $form_state) {

  $config = $this->config('image_style_folder.settings');

  $form['image_style_fs'] = array(
    '#type' => 'fieldset',
    '#title' => $this->t('Path'),
    '#description' => $this->t('Path settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['image_style_fs']['image_style_folder_path'] = array(
    '#type' => 'textfield',
    '#title' => $this->t('Path to generate to (add trailing slash!)'),
    '#description' => $this->t('Enter the path you want the image styles be generated to. Private file path is recommended.'),
    '#default_value' => $config->get('image_style_folder_path'),
    '#required' => TRUE,
  );

  $node_types = node_type_get_names();
  $selected_node_types = $config->get('image_style_folder_node_type');
  $form['image_style_folder_node_type'] = array(
    '#type' => 'select',
    '#title' => $this->t('Node type'),
    '#description' => $this->t('Run on this node type'),
    '#options' => $node_types,
    '#default_value' => !empty($selected_node_types) ? $selected_node_types : 'page', //page is a core type so use it as default
    '#required' => false,
  );

  $selected_field = $config->get('image_style_folder_field');
  $form['image_style_folder_field'] = array(
    '#type' => 'textfield',
    '#title' => $this->t('Field (machine name!)'),
    '#description' => $this->t('Run on this field'),
    '#default_value' => !empty($selected_field) ? $selected_field : 'Your fields machine name here!', //page is a core type so use it as default
    '#required' => TRUE,
  );


  $overwrite = $config->get('image_style_folder_overwrite');
  $form['image_style_fs']['image_style_folder_overwrite'] = array(
    '#type' => 'checkbox',
    '#title' => $this->t('Overwrite'),
    '#description' => $this->t('Overwrite already existing images?'),
    '#default_value' => !empty($overwrite) ? $overwrite : 0,
    '#required' => false,
  );

  //list avaliable image styles
  $imageStyleOptions = image_style_options(FALSE);
  $initialImageStyles = $config->get('initial_image_styles');
  $form['initial_image_styles'] = [
    '#type' => 'checkboxes',
    '#title' => $this->t('Initial image styles'),
    '#description' => $this->t('Select image styles which will be created initial for an image.'),
    '#options' => $imageStyleOptions,
    '#default_value' => !empty($initialImageStyles) ? $initialImageStyles : [],
    '#size' => 10,
  ];

  return parent::buildForm($form, $form_state);
}

function validateForm(array &$form, FormStateInterface $form_state) {

}

function submitForm(array &$form, FormStateInterface $form_state) {
  $config = $this->config('image_style_folder.settings');

  //all manually...just ugly. There has to be a better way.
  $config
    ->set('image_style_folder_path', $form_state->getValue('image_style_folder_path'))
    ->set('initial_image_styles', $form_state->getValue('initial_image_styles'))
    ->set('image_style_folder_overwrite', $form_state->getValue('image_style_folder_overwrite'))
    ->set('image_style_folder_node_type', $form_state->getValue('image_style_folder_node_type'))
    ->set('image_style_folder_field', $form_state->getValue('image_style_folder_field'))
    ->save();

  parent::submitForm($form, $form_state);
}


}
